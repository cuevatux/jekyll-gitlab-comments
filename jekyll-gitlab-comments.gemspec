Gem::Specification.new do |spec|
  spec.name        = 'jekyll-gitlab-comments'
  spec.version     = '1.0.1'
  spec.date        = '2016-11-23'
  spec.summary     = "Jekyll Gitlab Comments"
  spec.description = "A Jekyll plugin to provide static site comments via Gitlab pull requests."
  spec.authors     = ["Steve Le Roy Harris"]
  spec.email       = 'steve@nourish.je'
  spec.files       = ["lib/jekyll-gitlab-comments.rb"]
  spec.homepage    =
    'http://rubygems.org/gems/jekyll-github-comments'
  spec.license       = 'MIT'
  spec.add_runtime_dependency "jekyll", "~> 3.1"
  spec.add_runtime_dependency "liquid", "~> 3.0"
end
